/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evan.remon.system.readers.objects;

/**
 *
 * @author surreal
 */
public class CpuInformation {
    
    private String model;
    private Integer frequency;
    private Integer coreCount;
    private Float vcore;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    public Integer getCoreCount() {
        return coreCount;
    }

    public void setCoreCount(Integer coreCount) {
        this.coreCount = coreCount;
    }

    public Float getVcore() {
        return vcore;
    }

    public void setVcore(Float vcore) {
        this.vcore = vcore;
    }
    
}
