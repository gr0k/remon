/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evan.remon.system.readers;

import com.evan.remon.system.readers.objects.GpuInformation;
import com.evan.remon.system.readers.objects.GpuSensors;

/**
 *
 * @author surreal
 */
public interface GpuReader {
    
    public GpuInformation getGpuInformation();
    
    public GpuSensors getGpuSensors();
    
}
