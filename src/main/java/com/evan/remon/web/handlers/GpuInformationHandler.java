/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evan.remon.web.handlers;

import com.evan.remon.system.SystemReader;
import com.evan.remon.system.readers.objects.GpuInformation;
import com.evan.remon.web.RequestHandler;

/**
 *
 * @author surreal
 */
public class GpuInformationHandler implements RequestHandler {
    
    SystemReader reader;
    
    public GpuInformationHandler(SystemReader reader) {
        this.reader = reader;
    }
    
    @Override
    public GpuInformation handleRequest() {
        return reader.getGpuInformation();
    }
    
}
