/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evan.remon.system.readers;

import com.evan.remon.system.readers.objects.CpuInformation;
import com.evan.remon.system.readers.objects.GpuInformation;
import com.evan.remon.system.readers.objects.RamInformation;

/**
 *
 * @author surreal
 */
public class SystemInformation {
    
    private CpuInformation cpuInfo;
    private GpuInformation gpuInfo;
    private RamInformation ramInfo;

    public CpuInformation getCpuInfo() {
        return cpuInfo;
    }

    public void setCpuInfo(CpuInformation cpuInfo) {
        this.cpuInfo = cpuInfo;
    }

    public GpuInformation getGpuInfo() {
        return gpuInfo;
    }

    public void setGpuInfo(GpuInformation gpuInfo) {
        this.gpuInfo = gpuInfo;
    }

    public RamInformation getRamInfo() {
        return ramInfo;
    }

    public void setRamInfo(RamInformation ramInfo) {
        this.ramInfo = ramInfo;
    }
    
}
