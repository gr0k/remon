/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evan.remon.system.readers.objects;

import com.sun.management.OperatingSystemMXBean;
import java.lang.management.ManagementFactory;
import java.util.List;

/**
 *
 * @author surreal
 */
public class CpuSensors {
    
    private List<Float> temperature;
    private List<Integer> load;

    public List<Float> getTemperature() {
        return temperature;
    }

    public void setTemperature(List<Float> temperature) {
        this.temperature = temperature;
    }

    public List<Integer> getLoad() {
        return load;
    }

    public void setLoad(List<Integer> load) {
        this.load = load;
    }
    
}
