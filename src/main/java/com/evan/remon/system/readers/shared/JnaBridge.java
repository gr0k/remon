/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evan.remon.system.readers.shared;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinNT;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.win32.W32APIOptions;

/**
 *
 * @author surreal
 */
public class JnaBridge {
    
     Kernel32Jna kernel32;
     
     public JnaBridge() {
         kernel32 = (Kernel32Jna) Native.loadLibrary("Kernel32", Kernel32Jna.class, W32APIOptions.UNICODE_OPTIONS);
     }
     
     public Pointer openAndMapSharedMem(String memName) {
         HANDLE openMap = kernel32.OpenFileMapping(Kernel32Jna.FILE_MAP_READ,
                false,
                memName);

        if (openMap == null) {
            return null;
        }

        Pointer ptr = kernel32.MapViewOfFile(openMap,
                Kernel32Jna.FILE_MAP_READ,
                0,
                0,
                0);

        kernel32.CloseHandle(openMap);
        
        if (ptr == null) {
            return null;
        }
        
        return ptr;
     }
    
}
