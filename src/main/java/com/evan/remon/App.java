package com.evan.remon;

import com.evan.remon.system.SystemReader;
import com.evan.remon.system.readers.CoreTempCpuReader;
import com.evan.remon.system.readers.GpuReader;
import com.evan.remon.system.readers.GpuzGpuReader;
import com.evan.remon.system.readers.CpuReader;
import com.evan.remon.system.readers.NativeRamReader;
import com.evan.remon.system.readers.RamReader;
import com.evan.remon.system.readers.SystemInformation;
import com.evan.remon.system.readers.objects.CpuInformation;
import com.evan.remon.system.readers.objects.CpuSensors;
import com.evan.remon.system.readers.objects.GpuInformation;
import com.evan.remon.system.readers.objects.GpuSensors;
import com.evan.remon.system.readers.objects.RamInformation;
import com.evan.remon.system.readers.objects.SystemSensors;
import com.evan.remon.web.RemonServer;
import com.evan.remon.web.RequestHandler;
import com.evan.remon.web.handlers.CpuInformationHandler;
import com.evan.remon.web.handlers.CpuSensorsHandler;
import com.evan.remon.web.handlers.GpuInformationHandler;
import com.evan.remon.web.handlers.GpuSensorsHandler;
import com.evan.remon.web.handlers.RamInformationHandler;
import com.evan.remon.web.handlers.SystemInformationHandler;
import com.evan.remon.web.handlers.SystemSensorsHandler;
import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Platform;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.win32.StdCallLibrary;
import com.sun.jna.win32.W32APIOptions;
import com.sun.management.OperatingSystemMXBean;
import java.lang.management.ManagementFactory;
import java.util.List;


/**
 * Hello world!
 *
 */
public class App {

    public static void main(String[] args) throws Exception {
        RemonServer server = new RemonServer();
        SystemReader reader = new SystemReader();
        
        reader.setCpuReader(new CoreTempCpuReader());
        reader.setGpuReader(new GpuzGpuReader());
        reader.setRamReader(new NativeRamReader());
        
        RequestHandler<CpuInformation> cpuInfoHandler 
                = new CpuInformationHandler(reader);
        server.addRequestHandler(cpuInfoHandler, "/cpuInformation");
        
        RequestHandler<CpuSensors> cpuSensorHandler
                = new CpuSensorsHandler(reader);
        server.addRequestHandler(cpuSensorHandler, "/cpuSensors");
        
        RequestHandler<GpuInformation> gpuInfoHandler
                = new GpuInformationHandler(reader);
        server.addRequestHandler(gpuInfoHandler, "/gpuInformation");
        
        RequestHandler<GpuSensors> gpuSensorsHandler
                = new GpuSensorsHandler(reader);
        server.addRequestHandler(gpuSensorsHandler, "/gpuSensors");
        
        RequestHandler<RamInformation> ramInfoHandler
                = new RamInformationHandler(reader);
        server.addRequestHandler(ramInfoHandler, "/ramInformation");
        
        RequestHandler<SystemInformation> sysInfoHandler
                = new SystemInformationHandler(reader);
        server.addRequestHandler(sysInfoHandler, "/systemInformation");
        
        RequestHandler<SystemSensors> sysSensorsHandler
                = new SystemSensorsHandler(reader);
        server.addRequestHandler(sysSensorsHandler, "/systemSensors");
        
        server.start();
    }
    
}
