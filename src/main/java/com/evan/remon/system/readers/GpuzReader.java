/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evan.remon.system.readers;

import com.sun.jna.Pointer;
import java.nio.charset.Charset;

/**
 *
 * @author surreal
 */
public class GpuzReader {
    
    private static final int SIZE_GPUZRECORD = 1024;
    private static final int SIZE_GPUZRECORD_KEY = 512;
    private static final int SIZE_GPUZRECORD_VALUE = 512;
    private static final int SIZE_GPUZSENSOR = 256*2 + 8*2 + 4  + 8;
    
    private static final int OFFSET_DATA = 4 + 4 + 4;
    private static final int OFFSET_SENSORS = (OFFSET_DATA + SIZE_GPUZRECORD*128);
    private static final int OFFSET_GPUZRECORD_VALUE = (256+8)*2;
    private static final int OFFSET_SENSORVALUES = OFFSET_SENSORS + OFFSET_GPUZRECORD_VALUE + 4; // Align bug

    private static final int OFFSET_DATAVAL_CORECLOCK = OFFSET_DATA + SIZE_GPUZRECORD_KEY + (SIZE_GPUZRECORD * 1);
    private static final int OFFSET_DATAVAL_MEMSIZE = OFFSET_DATA + SIZE_GPUZRECORD_KEY + (SIZE_GPUZRECORD * 43);
    private static final int OFFSET_DATAVAL_CARDNAME = OFFSET_DATA + SIZE_GPUZRECORD_KEY + (SIZE_GPUZRECORD * 68);
    private static final int OFFSET_DATAVAL_MEMCLOCK = OFFSET_DATA + SIZE_GPUZRECORD_KEY + (SIZE_GPUZRECORD * 69);
    
    private static final int OFFSET_SENSORVAL_GPUTEMP = (OFFSET_SENSORVALUES + SIZE_GPUZSENSOR*2);
    private static final int OFFSET_SENSORVAL_MEMUSED = (OFFSET_SENSORVALUES + SIZE_GPUZSENSOR*5);
    private static final int OFFSET_SENSORVAL_LOAD = (OFFSET_SENSORVALUES + SIZE_GPUZSENSOR*6);
    private static final int OFFSET_SENSORVAL_VCORE = (OFFSET_SENSORVALUES + SIZE_GPUZSENSOR*10);
    
    Pointer ptr;
    
    private double getSensorValue(int offset) {
        return (double)Math.round(ptr.getDouble(offset)*100)/100;
    }
    
    public String getDataValue(int offset) {
        return ptr.getWideString(offset);
    }
    
    public GpuzReader(Pointer ptr) {
        this.ptr = ptr;
    }
    
    public String getAllKeys() {
        
        Pointer base, key, value;
        String keyString, valueString;
        for(int x = 0; x < 100; x++) {
            base = ptr.share(x * SIZE_GPUZRECORD + OFFSET_DATA);
            key = base;
            value = key.share(512);
            
            keyString = key.getWideString(0);
            if(keyString.isEmpty())
                break;
            
            valueString = value.getWideString(0);
            System.out.println(keyString + ": " + valueString);
        }
        
        return ptr.getString(0);
    }
    
    public String getGpuModel() {
        return ptr.getWideString(OFFSET_DATAVAL_CARDNAME);
    }
    
    public int getCoreClock() {
        return Integer.parseInt(getDataValue(OFFSET_DATAVAL_CORECLOCK));
    }
    
    public int getMemClock() {
        return Integer.parseInt(getDataValue(OFFSET_DATAVAL_MEMCLOCK));
    }
    
    public int getMemSize() {
        return Integer.parseInt(getDataValue(OFFSET_DATAVAL_MEMSIZE));
    }
    
    public double getGpuTemp() {
        return getSensorValue(OFFSET_SENSORVAL_GPUTEMP);
    }
    
    public double getMemUsed() {
        return getSensorValue(OFFSET_SENSORVAL_MEMUSED);
    }
    
    public double getLoad() {
        return getSensorValue(OFFSET_SENSORVAL_LOAD);
    }
    
    public double getVoltage() {
        return getSensorValue(OFFSET_SENSORVAL_VCORE);
    }
    
}
