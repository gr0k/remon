
package com.evan.remon.web;

import com.evan.remon.App;
import com.evan.remon.system.readers.objects.HandlerError;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.glassfish.grizzly.http.server.CLStaticHttpHandler;
import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.NetworkListener;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;
import org.glassfish.grizzly.http.server.StaticHttpHandler;

/**
 * Main HTTP server implementation to handle api and web requests
 * @author surreal
 */
public class RemonServer {
 
    HttpServer server;
    
    public RemonServer() {
        server = new HttpServer();
        server.addListener(new NetworkListener("remonListener", "0.0.0.0", 80));
        server.getServerConfiguration().addHttpHandler(new CLStaticHttpHandler(App.class.getClassLoader()));
    }
    
    public void addRequestHandler(final RequestHandler handler, String uri) {
        server.getServerConfiguration().addHttpHandler(
    new HttpHandler() {
        final ObjectMapper mapper = new ObjectMapper();
        
        @Override
        public void service(Request request, Response response) throws Exception {
            
            Object result = null;
            try {
                result = handler.handleRequest();
            }
            catch(Exception e) {
                HandlerError err = new HandlerError();
                err.setErrorMessage("Error retrieving data from '" + handler.getClass().getSimpleName() + "'");
                result = err;
            }
            
            
            String json = mapper.writeValueAsString(result);

            response.setContentType("application/json");
            response.setContentLength(json.length());
            response.getWriter().write(json);
        }
    },
    uri);
    }
    
    public void start() throws Exception {
        server.start();
        System.out.println("Press any key to stop the server...");
        System.in.read();
    }
    
}
