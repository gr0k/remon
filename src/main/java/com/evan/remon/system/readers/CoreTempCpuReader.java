package com.evan.remon.system.readers;

import com.evan.remon.system.readers.shared.Kernel32Jna;
import com.evan.remon.system.readers.objects.CpuInformation;
import com.evan.remon.system.readers.objects.CpuSensors;
import com.evan.remon.system.readers.shared.JnaBridge;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.win32.W32APIOptions;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * CpuReader implementation backed by CoreTemp as the source Using their
 * shared memory model via JNA
 *
 * @author surreal
 */
public class CoreTempCpuReader implements CpuReader {

    CoreTempReader cReader;

    public CoreTempCpuReader() {
        cReader = initCoreTempConnect();
    }

    private CoreTempReader initCoreTempConnect() {
        JnaBridge bridge = new JnaBridge();
        Pointer ptr = bridge.openAndMapSharedMem("CoreTempMappingObjectEx");
        
        return new CoreTempReader(ptr);
    }

    @Override
    public CpuInformation getCpuInformation() {
        CpuInformation info = new CpuInformation();

        info.setCoreCount(cReader.getCpuCoreCount());
        info.setFrequency((int)cReader.getCpuSpeed());
        info.setModel(cReader.getCpuName());
        info.setVcore(cReader.getVcore());
        
        return info;
    }

    @Override
    public CpuSensors getCpuSensors() {
        CpuSensors sensors = new CpuSensors();
        
        float[] temps = cReader.getCpuTemp();
        List<Float> tempList = new ArrayList<>(temps.length);
        for(float temp : temps)
            tempList.add(temp);
        sensors.setTemperature(tempList);
        
        int[] loads = cReader.getCpuLoad();
        List<Integer> loadList = new ArrayList<>(loads.length);
        for(int load : loads)
            loadList.add(load);
        sensors.setLoad(loadList)
                ;
        return sensors;
    }
    
}
