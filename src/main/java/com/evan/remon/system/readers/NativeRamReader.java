/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evan.remon.system.readers;

import com.evan.remon.system.readers.objects.RamInformation;
import com.sun.management.OperatingSystemMXBean;
import java.lang.management.ManagementFactory;

/**
 *
 * @author surreal
 */
public class NativeRamReader implements RamReader {
    
    private OperatingSystemMXBean os;
    
    public NativeRamReader() {
        os = (com.sun.management.OperatingSystemMXBean)ManagementFactory.getOperatingSystemMXBean();
    }
    
    @Override
    public RamInformation getRamInformation() {
        RamInformation info = new RamInformation();
        
        info.setTotalMemory((int)(os.getTotalPhysicalMemorySize() / 1024 / 1024));
        info.setFreeMemory((int)(os.getFreePhysicalMemorySize() / 1024 / 1024));
        
        return info;
    }
    
}
