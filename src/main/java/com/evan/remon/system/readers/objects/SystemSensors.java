/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evan.remon.system.readers.objects;

/**
 *
 * @author surreal
 */
public class SystemSensors {
    
    private CpuSensors cpuSensors;
    private GpuSensors gpuSensors;

    public CpuSensors getCpuSensors() {
        return cpuSensors;
    }

    public void setCpuSensors(CpuSensors cpuSensors) {
        this.cpuSensors = cpuSensors;
    }

    public GpuSensors getGpuSensors() {
        return gpuSensors;
    }

    public void setGpuSensors(GpuSensors gpuSensors) {
        this.gpuSensors = gpuSensors;
    }
    
}
