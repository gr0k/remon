var app = angular.module('app', []);

app.service('dataProvider', function($http, alertProvider) {
    var service = {};
    var ignoreList = {};
    
    this.getData = function(name, callback) {
        $http.get('/' + name).
        success(function(data) {
            if(data.errorMessage) {
                console.log(ignoreList[name]);
              
                if(ignoreList[name] !== true) {
                    console.log("name was false " + name);
                    ignoreList[name] = true; // Service down, ignore alerts
                    alertProvider.error(data.errorMessage);
                }
            }
            else {
                // Reset alert ignore counter now that were up again
                if(ignoreList['all'])
                    ignoreList['all'] = false;
                
                callback(data);
            }
        }).
        error(function(data) {
            if(!ignoreList['all'])
                alertProvider.alert("Lost connection to ReMon server");
            
            ignoreList['all'] = true;
        });
   };
  
});

app.factory('alertProvider', function() {
    return alertify; // Implements everything we need, maintain wrapper
});

app.factory('chartProvider', function() {
    var service = {};
    
    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });
    
    service.initChart = function(chartOptions) {
        var chart = new Highcharts.Chart({
            chart: {
                renderTo: chartOptions.divId,
                type: chartOptions.type,
                zoomType: 'x'
            },
            exporting: {
              filename: chartOptions.title,
              width: screen.width
            },
            title: {
                text: chartOptions.title
            },
            xAxis: {
                type: 'datetime',
                gridLineWidth: 1,
                minTickInterval: 10000,
                tickInterval: 60000,
                dateTimeLabelFormats: {
                    millisecond: '%l:%M',
                    second: '%l:%M',
                    minute: '%l:%M'
                }
            },
            yAxis: chartOptions.yAxis,
            tooltip: {
                shared: true
            },
            series: chartOptions.seriesData
        });
        
        return chart;
    };
    
    service.addSeries = function(chart, type, title, suffix) {
        chart.addSeries({type: type, tooltip: { valueSuffix: suffix }, pointInterval: 1000, name: title, data: [], marker: {radius: 0}});
    };
    
    service.addData = function(data, series, offset) {
        if (typeof data === "object")
            for (x = 0; x < data.length; x++)
                this.addData(data[x], series[x + (offset ? offset : 0)]);
        else
            series.addPoint([(new Date().getTime()), data], false, (series.data.length > 360));

    };
    
    return service;
});

app.controller('homeController', 
    function($scope, $interval, dataProvider, chartProvider) {
        $scope.cpuInfo = {};
        $scope.cpuSensors = {};
        $scope.gpuInfo = {};
        $scope.gpuSensors = {};
        $scope.ramInfo = {};
        
        var cpuChart = {};
        var gpuChart = {};
        
        var cpuChartOptions = {
            divId: 'cpu-chart',
            type: 'line',
            title: 'CPU Statistics',
            yAxis: {
                title: {
                    text: 'Degrees C'
                },
                max: 100
            },
            seriesData: [
                /* Dynamically filled by CPU count data */
            ]
        };
        cpuChart = chartProvider.initChart(cpuChartOptions);
        
        var gpuChartOptions = {
            divId: 'gpu-chart',
            type: 'line',
            title: 'GPU Statistics',
            yAxis: {
                title: {
                    text: 'Degrees C'
                },
                max: 100
            },
            seriesData: [
                /* Added using utility wrapper */
            ]
        };
        gpuChart = chartProvider.initChart(gpuChartOptions);
        chartProvider.addSeries(gpuChart, 'line', 'GPU 0', 'C');
        chartProvider.addSeries(gpuChart, 'column', 'GPU 0', '%');
        
        /* Static data that doesn't change */
        dataProvider.getData('cpuInformation', function(data) {
            $scope.cpuInfo = data;
        });

        dataProvider.getData('gpuInformation', function(data) {
            $scope.gpuInfo = data;
        });
        
        /* Update stats every second */
        $interval(function() {
            dataProvider.getData('cpuSensors', function(data) {
                $scope.cpuSensors = data;
            });

            dataProvider.getData('gpuSensors', function(data) {
                $scope.gpuSensors = data;
            });

            dataProvider.getData('ramInformation', function(data) {
                $scope.ramInfo = data;
            });
        }, 1000);
        
        /* Update sensor graph data every 5 seconds */
        $interval(function() {
            /* Update CPU data */
            var seriesCount = cpuChart.series.length;
            var cpuCount = $scope.cpuInfo.coreCount;

            // Create N number of CPU graph series for cpu load
            if (seriesCount < cpuCount) {
                for (x = 0; x < $scope.cpuSensors.load.length; x++)
                    chartProvider.addSeries(cpuChart, 'line', 'CPU ' + x, 'C');
            }
            chartProvider.addData($scope.cpuSensors.temperature, cpuChart.series);

            // Create N number of GPU graph series for cpu load
            if (seriesCount < (cpuCount * 2)) {
                for (x = 0; x < cpuCount; x++)
                    chartProvider.addSeries(cpuChart, 'column', 'CPU ' + x, '%');
            }
            chartProvider.addData($scope.cpuSensors.load, cpuChart.series, cpuCount);
            
            /* Update GPU data */
            chartProvider.addData($scope.gpuSensors.temperature, gpuChart.series[0]);
            chartProvider.addData($scope.gpuSensors.load, gpuChart.series[1]);
            
            // Finally update charts */
            cpuChart.redraw();
            gpuChart.redraw();
        }, 5000);
    }
);
    
