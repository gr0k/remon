/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evan.remon.system.readers;

import com.evan.remon.system.readers.objects.CpuInformation;
import com.evan.remon.system.readers.objects.CpuSensors;
import java.util.List;

/**
 *
 * @author surreal
 */
public interface CpuReader {
    
    public CpuInformation getCpuInformation();
    
    public CpuSensors getCpuSensors();
    
}
