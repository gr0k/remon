/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evan.remon.system.readers;

import com.evan.remon.system.readers.objects.GpuInformation;
import com.evan.remon.system.readers.objects.GpuSensors;
import com.evan.remon.system.readers.shared.JnaBridge;
import com.sun.jna.Pointer;

/**
 *
 * @author surreal
 */
public class GpuzGpuReader implements GpuReader {
    
    GpuzReader reader;
    
    public GpuzGpuReader() {
        JnaBridge bridge = new JnaBridge();
        Pointer ptr = bridge.openAndMapSharedMem("GPUZShMem");
        if(ptr == null) {
            System.out.println("GPU-Z Not running!");
        }
        
        reader = new GpuzReader(ptr);
    }
    
    @Override
    public GpuInformation getGpuInformation() {
        GpuInformation info = new GpuInformation();
        
        info.setGpuName(reader.getGpuModel());
        info.setCoreClock(reader.getCoreClock());
        info.setMemClock(reader.getMemClock());
        info.setTotalMemory(reader.getMemSize());
        
        return info;
    }
    
    @Override
    public GpuSensors getGpuSensors() {
        GpuSensors sensors = new GpuSensors();
        
        sensors.setLoad(reader.getLoad());
        sensors.setMemoryUsage(reader.getMemUsed());
        sensors.setTemperature(reader.getGpuTemp());
        sensors.setVoltage(reader.getVoltage());
        
        return sensors;
    }
}
