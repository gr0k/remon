/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evan.remon.web;

/**
 *
 * @author surreal
 */
public interface RequestHandler<T> {

    public T handleRequest();

}
