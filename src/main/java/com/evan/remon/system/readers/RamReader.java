/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evan.remon.system.readers;

import com.evan.remon.system.readers.objects.RamInformation;

/**
 *
 * @author surreal
 */
public interface RamReader {
    
    public RamInformation getRamInformation();
    
}
