/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evan.remon.web.handlers;

import com.evan.remon.system.SystemReader;
import com.evan.remon.system.readers.objects.SystemSensors;
import com.evan.remon.web.RequestHandler;

/**
 *
 * @author surreal
 */
public class SystemSensorsHandler implements RequestHandler {
    
    private SystemReader reader;
    
    public SystemSensorsHandler(SystemReader reader) {
        this.reader = reader;
    }
    
    @Override
    public SystemSensors handleRequest() {
        SystemSensors sensors = new SystemSensors();
        
        sensors.setCpuSensors(reader.getCpuSensors());
        sensors.setGpuSensors(reader.getGpuSensors());
        
        return sensors;
    }
}
