/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evan.remon.web.handlers;

import com.evan.remon.system.SystemReader;
import com.evan.remon.system.readers.objects.CpuInformation;
import com.evan.remon.web.RequestHandler;

/**
 *
 * @author surreal
 */
public class CpuInformationHandler implements RequestHandler {
    
    SystemReader reader;
    
    public CpuInformationHandler(SystemReader reader) {
        this.reader = reader;
    }
    
    @Override
    public CpuInformation handleRequest() {
        return reader.getCpuInformation();
    }
    
}
