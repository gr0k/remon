/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evan.remon.system.readers;

import com.sun.jna.Pointer;
import java.text.DecimalFormat;

/**
 * Retrieves live data from shared CoreTemp memory
 * Allows us to grab up to date data without creating new objects
 * @author surreal
 */
public class CoreTempReader {
    
    private static final int SIZE_INT = 4;
    private static final int SIZE_FLOAT = 4;
    private static final int SIZE_BYTE = 1;
    
    private static final int OFFSET_LOAD = 0;
    private static final int OFFSET_CORECOUNT = (SIZE_INT * (256 + 128));
    private static final int OFFSET_TEMP = OFFSET_CORECOUNT + (SIZE_INT * 2);
    private static final int OFFSET_VCORE = OFFSET_TEMP + (SIZE_FLOAT * 256);
    private static final int OFFSET_CPUSPEED = OFFSET_VCORE + (SIZE_FLOAT * 1);
    private static final int OFFSET_CPUNAME = OFFSET_CPUSPEED + (SIZE_FLOAT * 3);
            
    private Pointer pointer;
    
    public CoreTempReader(Pointer pointer) {
        this.pointer = pointer;
    }
    
    public float getCpuSpeed() {
        return pointer.getFloat(OFFSET_CPUSPEED);
    }
    
    public int[] getCpuLoad() {
        return pointer.getIntArray(OFFSET_LOAD, getCpuCoreCount());
    }
    
    public int getCpuCoreCount() {
        return pointer.getInt(OFFSET_CORECOUNT);
    }
    
    public String getCpuName() {
        return pointer.getString(OFFSET_CPUNAME);
    }
    
    public float getVcore() {
        return (float)Math.round(pointer.getFloat(OFFSET_VCORE)*100)/100;
    }

    public float[] getCpuTemp() {
        return pointer.getFloatArray(OFFSET_TEMP, getCpuCoreCount());
    }

}
