/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evan.remon.web.handlers;

import com.evan.remon.system.SystemReader;
import com.evan.remon.system.readers.objects.CpuSensors;
import com.evan.remon.web.RequestHandler;

/**
 *
 * @author surreal
 */
public class CpuSensorsHandler implements RequestHandler {
    
    SystemReader reader;
    
    public CpuSensorsHandler(SystemReader reader) {
        this.reader = reader;
    }
    
    @Override
    public CpuSensors handleRequest() {
        return reader.getCpuSensors();
    }
    
}
