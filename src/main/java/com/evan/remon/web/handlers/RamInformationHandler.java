/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evan.remon.web.handlers;

import com.evan.remon.system.SystemReader;
import com.evan.remon.system.readers.objects.RamInformation;
import com.evan.remon.web.RequestHandler;

/**
 *
 * @author surreal
 */
public class RamInformationHandler implements RequestHandler {
    
    SystemReader reader;
    
    public RamInformationHandler(SystemReader reader) {
        this.reader = reader;
    }
    
    @Override
    public RamInformation handleRequest() {
        return reader.getRamInformation();
    }
    
}
