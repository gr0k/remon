/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evan.remon.web.handlers;

import com.evan.remon.system.SystemReader;
import com.evan.remon.system.readers.objects.GpuSensors;
import com.evan.remon.web.RequestHandler;

/**
 *
 * @author surreal
 */
public class GpuSensorsHandler implements RequestHandler {
    
    SystemReader reader;
    
    public GpuSensorsHandler(SystemReader reader) {
        this.reader = reader;
    }
    
    @Override
    public GpuSensors handleRequest() {
        return reader.getGpuSensors();
    }
    
}
