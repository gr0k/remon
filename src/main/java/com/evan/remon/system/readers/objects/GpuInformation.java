/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evan.remon.system.readers.objects;

/**
 *
 * @author surreal
 */
public class GpuInformation {
    
    private String gpuName;
    private int coreClock;
    private int memClock;
    private int totalMemory;

    public String getGpuName() {
        return gpuName;
    }

    public void setGpuName(String gpuName) {
        this.gpuName = gpuName;
    }

    public int getCoreClock() {
        return coreClock;
    }

    public void setCoreClock(int coreClock) {
        this.coreClock = coreClock;
    }

    public int getMemClock() {
        return memClock;
    }

    public void setMemClock(int memClock) {
        this.memClock = memClock;
    }

    public int getTotalMemory() {
        return totalMemory;
    }

    public void setTotalMemory(int totalMemory) {
        this.totalMemory = totalMemory;
    }
    
}
