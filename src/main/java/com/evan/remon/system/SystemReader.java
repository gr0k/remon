
package com.evan.remon.system;

import com.evan.remon.system.readers.CpuReader;
import com.evan.remon.system.readers.GpuReader;
import com.evan.remon.system.readers.RamReader;
import com.evan.remon.system.readers.objects.CpuInformation;
import com.evan.remon.system.readers.objects.CpuSensors;
import com.evan.remon.system.readers.objects.GpuInformation;
import com.evan.remon.system.readers.objects.GpuSensors;
import com.evan.remon.system.readers.objects.RamInformation;

/**
 * Primary class for retrieving system information
 * @author surreal
 */
public class SystemReader {
    
    private CpuReader cpuReader;
    private GpuReader gpuReader;
    private RamReader ramReader;
    
    public CpuReader getCpuReader() {
        return cpuReader;
    }

    public void setCpuReader(CpuReader cpuReader) {
        this.cpuReader = cpuReader;
    }

    public GpuReader getGpuReader() {
        return gpuReader;
    }

    public void setGpuReader(GpuReader gpuReader) {
        this.gpuReader = gpuReader;
    }

    public RamReader getRamReader() {
        return ramReader;
    }

    public void setRamReader(RamReader ramReader) {
        this.ramReader = ramReader;
    }
    
    public CpuInformation getCpuInformation() {
        return cpuReader.getCpuInformation();
    }
    
    public CpuSensors getCpuSensors() {
        return cpuReader.getCpuSensors();
    }
    
    public GpuInformation getGpuInformation() {
        return gpuReader.getGpuInformation();
    }
    
    public GpuSensors getGpuSensors() {
        return gpuReader.getGpuSensors();
    }
    
    public RamInformation getRamInformation() {
        return ramReader.getRamInformation();
    }
    
}
