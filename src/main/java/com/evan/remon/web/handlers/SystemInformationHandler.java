/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evan.remon.web.handlers;

import com.evan.remon.system.SystemReader;
import com.evan.remon.system.readers.SystemInformation;
import com.evan.remon.web.RequestHandler;

/**
 *
 * @author surreal
 */
public class SystemInformationHandler implements RequestHandler {
    
    private SystemReader reader;
    
    public SystemInformationHandler(SystemReader reader) {
        this.reader = reader;
    }
    
    @Override
    public SystemInformation handleRequest() {
        SystemInformation info = new SystemInformation();
        
        info.setCpuInfo(reader.getCpuInformation());
        info.setGpuInfo(reader.getGpuInformation());
        info.setRamInfo(reader.getRamInformation());
        
        return info;
    }
    
}
