/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evan.remon.system.readers.shared;

import com.evan.remon.App;
import com.sun.jna.Native;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.WinNT;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.win32.StdCall;
import com.sun.jna.win32.W32APIOptions;

/**
 *
 * @author surreal
 */
public interface Kernel32Jna extends StdCall, Kernel32 {
    
    static final int FILE_MAP_READ = 0x00000004;
    
    Kernel32 INSTANCE = (Kernel32)
            Native.loadLibrary("Kernel32", Kernel32Jna.class, W32APIOptions.UNICODE_OPTIONS);

        HANDLE OpenFileMapping(int access, Boolean inherot, String name);
    
}
